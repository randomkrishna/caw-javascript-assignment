let ToDo;
window.onload = function () {
  let todoTitle = document.getElementById('todoTitle');
  let todoDesc = document.getElementById('todoDesc');
  let quickAddTitle = document.getElementById('quickAddTitle');
  ToDo = new ToDoClass(todoTitle, todoDesc, quickAddTitle);
  ToDo.showExistingTodo();
};

class ToDoClass {
  constructor(todoTitle, todoDesc, quickAddTitle) {
    this.todoTitle = todoTitle;
    this.todoDesc = todoDesc;
    this.quickAddTitle = quickAddTitle;
    this.modal = document.querySelector('#modal');
    this.localStorageKey = 'todoApp';
  }

  get listFromLocalStorage() {
    return JSON.parse(localStorage.getItem(this.localStorageKey));
  }

  setItemsToLocalStorage() {
    if (!this.todoTitle.value && !this.quickAddTitle.value) return; // check if not empty string
    const existingItems = this.listFromLocalStorage || [];
    const newTodo = {
      todoTitle: this.todoTitle.value || this.quickAddTitle.value,
      todoDesc: this.todoDesc.value,
      completed: false,
      id: Date.now(),
    };
    localStorage.setItem(this.localStorageKey, JSON.stringify([...existingItems, newTodo]));
  }

  showModal() {
    this.modal.className = 'modal';
  }

  addTodo() {
    this.setItemsToLocalStorage();
    this.resetTodo();
    this.closeModal();
    this.showExistingTodo();
  }

  closeModal() {
    this.modal.className = 'modal-closed';
  }

  resetTodo() {
    this.todoTitle.value = '';
    this.todoDesc.value = '';
    this.quickAddTitle.value = '';
  }

  markCompleted(id, index) {
    const existingItems = this.listFromLocalStorage;
    existingItems[index].completed = true;
    localStorage.setItem(this.localStorageKey, JSON.stringify([...existingItems]));
    this.showExistingTodo();
  }

  deleteTodo(id, index) {
    const existingItems = this.listFromLocalStorage;
    const filteredItems = existingItems.filter((t) => t.id !== id);
    localStorage.setItem(this.localStorageKey, JSON.stringify([...filteredItems]));
    this.showExistingTodo();
  }

  createElement(tag, className = [], id, innerHTML) {
    const element = document.createElement(tag);
    className && className.length ? (element.className = className.join(' ').trim()) : null;
    id ? (element.id = id) : null;
    innerHTML ? (element.innerHTML = innerHTML) : null;
    return element;
  }

  showExistingTodo() {
    const existingItems = this.listFromLocalStorage || [];
    const ulList = document.getElementById('todoList');
    ulList.innerHTML = '';
    if (existingItems.length <= 0) {
      ulList.innerHTML = '<li>No Todos, Add One</li>';
      return;
    }

    existingItems.forEach((todo, index) => {
      const liEl = this.createElement('li');

      const completeBtn = this.createElement('button', ['completed-todo'], null, '&#10004;');
      completeBtn.onclick = () => {
        this.markCompleted(todo.id, index);
      };
      liEl.appendChild(completeBtn);
      const descBox = this.createElement('div', ['todo-text-container']);
      descBox.appendChild(
        this.createElement(
          'p',
          ['font18', todo.completed ? 'line-through' : ''],
          null,
          todo.todoTitle
        )
      );
      descBox.appendChild(
        this.createElement(
          'p',
          ['font14', todo.completed ? 'line-through' : ''],
          null,
          todo.todoDesc
        )
      );
      liEl.appendChild(descBox);

      const removeBtn = this.createElement('button', ['remove-todo'], null, '&#10006;');
      removeBtn.onclick = () => {
        this.deleteTodo(todo.id, index);
      };
      liEl.appendChild(removeBtn);
      ulList.appendChild(liEl);
    });
  }
}
